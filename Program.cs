using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOS_Logs
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = $"BOS log reader";
            ConfigurationSetter.SetConfiguration(null, null);
            SQL.GetTracks();
            SQL.DatabaseLists();
            SQL.GetMoves();
            SQL.BuildTrainList();
            SQL.BuildOSEventsList();
            BOSLogs.ReadBOSLogs();
            CADLogs.ReadCADLogs();
            //SQL.BuildOSEventsList();
            SQL.UpdateTrain();
            Excel.ExportData();
            Console.Beep();
            Console.Beep();
        }
    }
}
