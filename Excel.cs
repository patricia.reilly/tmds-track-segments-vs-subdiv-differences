using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static BOS_Logs.GlobalDeclarations;

namespace BOS_Logs
{
    public static class Excel
    {
        private static int row = 1;
        private static int METXrow = 1;
        private static int AMTKrow = 1;
        public static void ExportData()
        {
            try
            {
                //Check for directory, create if doesn't exist
                DateTime month = new DateTime(int.Parse(ConfigurationSetter.BOS_filter_year), int.Parse(ConfigurationSetter.BOS_filter_month), 01);
                var path = $"D:\\BRC\\Data";
                var report = $"BRC_Report-{month.ToString("MMMM")} {ConfigurationSetter.BOS_filter_year}-(Exported-{DateTime.Now.ToString("yyyy-MM-dd")}).xlsx";
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                //Check for file, rename if exists
                if (File.Exists($"{path}\\{report}"))
                {
                    var fileDateTime = File.GetCreationTime($"{path}\\{report}");
                    var reportNew = $"BRC_Report-{month.ToString("MMMM")} {ConfigurationSetter.BOS_filter_year}-(Eported-{DateTime.Now.ToString("yyyy -MM-dd")})({fileDateTime.ToString("HH.mm.ss")}).xlsx";
                    File.Move($"{path}\\{report}", $"{path}\\{reportNew}");
                }
                //create workbook
                var wb = new XLWorkbook();
                AddToTrains();
                CreateTrainList(wb);
                CreateMETXAMTKWorksheets(wb);
                CreateMovementWorksheets(wb);
                Create2080Worksheet(wb);
                Create2010Worksheet(wb);
                Create2083Worksheet(wb);
                Create2084Worksheet(wb);
                CreateCADTrainList(wb);
                CreateTracksWithNoIndications(wb);
                wb.SaveAs($"{path}\\{report}");
                if (ConfigurationSetter.OpenReportUponCompletion)
                {
                    var process = new Process
                    {
                        StartInfo =
                    {
                        FileName = $"{path}\\{report}"
                    }
                    };
                    System.Threading.Thread.Sleep(2000);
                    process.Start();
                }
            }
            catch (Exception ex)
            {
            }
        }
        private static void Create2010Worksheet(XLWorkbook wb)
        {
            row = 1;
            Console.Write($"Building 2010 List ");
            try
            {
                //add worksheet to workbook
                var _1stmsg = ListMessages.First(x => x.Contains("|2010|"));
                var _1stmsgParts = _1stmsg.Split('|');
                var ws = wb.Worksheets.Add("2010_Messages");
                wb.Worksheet("2010_Messages").Position = 6;
                ws.SheetView.FreezeRows(1);
                ws.Cells("A1:BB1").Style.Font.Bold = true;
                for (int col = 1; col < _1stmsgParts.Count() + 1; col++)
                {
                    if (col - 1 != 1)
                    {
                        if (col - 1 == 0)
                        {
                            ws.Cell(row, col).Value = "Date/Time";
                        }
                        else
                        {
                            ws.Cell(row, col).Value = _1stmsgParts[col - 1].Split('=')[0];
                        }
                    }
                }
                foreach (var msg in ListMessages.FindAll(e => e.Split('|')[1] == "2010"))
                {
                    var parts = msg.Split('|');
                    row++;
                    for (int col = 1; col < parts.Count() + 1; col++)
                    {
                        if (col - 1 != 1)
                        {
                            if (col - 1 == 0)
                            {
                                ws.Cell(row, col).Value = parts[col - 1];
                            }
                            else
                            {
                                ws.Cell(row, col).Value = parts[col - 1].Split('=')[1];
                            }
                        }
                    }
                }
                ws.Range(2, 1, row, 1).Style.NumberFormat.Format = "mm/DD/yyyy HH:mm:ss.0";
                ws.Columns(1, _1stmsgParts.Count()).AdjustToContents();
                Console.WriteLine($"completed.");
            }
            catch (Exception ex)
            {
            }
        }
        private static void Create2080Worksheet(XLWorkbook wb)
        {
            row = 1;
            Console.Write($"Building 2080 List ");
            try
            {
                //add worksheet to workbook
                var _1stmsg = ListMessages.First(x => x.Contains("|2080|"));
                var _1stmsgParts = _1stmsg.Split('|');
                var ws = wb.Worksheets.Add("2080_Messages");
                wb.Worksheet("2080_Messages").Position = 5;
                ws.SheetView.FreezeRows(1);
                ws.Cells("A1:BB1").Style.Font.Bold = true;
                for (int col = 1; col < _1stmsgParts.Count() + 1; col++)
                {
                    if (col - 1 != 1)
                    {
                        if (col - 1 == 0)
                        {
                            ws.Cell(row, col).Value = "Date/Time";
                        }
                        else
                        {
                            ws.Cell(row, col).Value = _1stmsgParts[col - 1].Split('=')[0];
                        }
                    }
                }
                int count = ListMessages.FindAll(e => e.Split('|')[1] == "2080").Count;
                foreach (var msg in List2080)
                {
                    var parts = msg.Split('|');
                    row++;
                    for (int col = 1; col < parts.Count() + 1; col++)
                    {
                        if (col - 1 != 1)
                        {
                            if (col - 1 == 0)
                            {
                                ws.Cell(row, col).Value = parts[col - 1];
                            }
                            else
                            {
                                ws.Cell(row, col).Value = parts[col - 1].Split('=')[1];
                            }
                        }
                    }
                }
                ws.Range(2, 1, row, 1).Style.NumberFormat.Format = "mm/DD/yyyy HH:mm:ss.0";
                ws.Columns(1, _1stmsgParts.Count()).AdjustToContents();
                Console.WriteLine($"completed.");
            }
            catch (Exception ex)
            {
            }
        }
        private static void Create2083Worksheet(XLWorkbook wb)
        {
            row = 1;
            Console.Write($"Building 2083 List ");
            try
            {
                //add worksheet to workbook
                var _1stmsg = ListMessages.First(x => x.Contains("|2083|"));
                var _1stmsgParts = _1stmsg.Split('|');
                var ws = wb.Worksheets.Add("2083_Messages");
                wb.Worksheet("2083_Messages").Position = 7;
                ws.SheetView.FreezeRows(1);
                ws.Cells("A1:BB1").Style.Font.Bold = true;
                for (int col = 1; col < _1stmsgParts.Count() + 1; col++)
                {
                    if (col - 1 != 1)
                    {
                        if (col - 1 == 0)
                        {
                            ws.Cell(row, col).Value = "Date/Time";
                        }
                        else
                        {
                            ws.Cell(row, col).Value = _1stmsgParts[col - 1].Split('=')[0];
                        }
                    }
                }
                foreach (var msg in ListMessages.FindAll(e => e.Split('|')[1] == "2083"))
                {
                    try
                    {
                        var parts = msg.Split('|');
                        row++;
                        for (int col = 1; col < parts.Count() + 1; col++)
                        {
                            if (col - 1 != 1)
                            {
                                if (col - 1 == 0)
                                {
                                    ws.Cell(row, col).Value = parts[col - 1];
                                }
                                else
                                {
                                    ws.Cell(row, col).Value = parts[col - 1].Split('=')[1];
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                    }
                }
                ws.Range(2, 1, row, 1).Style.NumberFormat.Format = "mm/DD/yyyy HH:mm:ss.0";
                ws.Columns(1, _1stmsgParts.Count()).AdjustToContents();
                Console.WriteLine($"completed.");
            }
            catch (Exception ex)
            {
            }
        }
        private static void Create2084Worksheet(XLWorkbook wb)
        {
            if (ListMessages.Any(x => x.Contains("|2084|")))
            {
                row = 1;
                Console.Write($"Building 2084 List ");
                try
                {
                    //add worksheet to workbook
                    var _1stmsg = ListMessages.First(x => x.Contains("|2084|"));
                    var _1stmsgParts = _1stmsg.Split('|');
                    var ws = wb.Worksheets.Add("2084_Messages");
                    wb.Worksheet("2084_Messages").Position = 8;
                    ws.SheetView.FreezeRows(1);
                    ws.Cells("A1:BB1").Style.Font.Bold = true;
                    for (int col = 1; col < _1stmsgParts.Count() + 1; col++)
                    {
                        if (col - 1 != 1)
                        {
                            if (col - 1 == 0)
                            {
                                ws.Cell(row, col).Value = "Date/Time";
                            }
                            else
                            {
                                ws.Cell(row, col).Value = _1stmsgParts[col - 1].Split('=')[0];
                            }
                        }
                    }
                    foreach (var msg in ListMessages.FindAll(e => e.Split('|')[1] == "2084"))
                    {
                        try
                        {
                            var parts = msg.Split('|');
                            row++;
                            for (int col = 1; col < parts.Count() + 1; col++)
                            {
                                if (col - 1 != 1)
                                {
                                    if (col - 1 == 0)
                                    {
                                        ws.Cell(row, col).Value = parts[col - 1];
                                    }
                                    else
                                    {
                                        ws.Cell(row, col).Value = parts[col - 1].Split('=')[1];
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                    ws.Range(2, 1, row, 1).Style.NumberFormat.Format = "mm/DD/yyyy HH:mm:ss.0";
                    ws.Columns(1, _1stmsgParts.Count()).AdjustToContents();
                    Console.WriteLine($"completed.");
                }
                catch (Exception ex)
                {
                }
            }
        }
        public static void AddToTrains()
        {
            foreach (var item in DictTrainsAdd)
            {
                ListTrains.Add(new tblTrains { Symbol = item.Key.ToString().Split('|')[0], EngineID = item.Key.ToString().Split('|')[1], CallTime = DateTime.Parse(item.Value.ToString().Split('|')[0]) });
            }
            ListTrains.OrderBy(x => x.CallTime);
        }
        private static void CreateMovementWorksheets(XLWorkbook wb)
        {
            row = 1;
            Console.Write($"Building Train Movement List ");
            try
            {
                //add worksheet to workbook
                var ws = wb.Worksheets.Add("tblTrainMovements");
                wb.Worksheet("tblTrainMovements").Position = 4;
                ws.SheetView.FreezeRows(1);
                ws.Cells("A1:BB1").Style.Font.Bold = true;
                ws.Cell($"A{row.ToString()}").Value = "EventTimeStamp";
                ws.Cell($"B{row.ToString()}").Value = "EventTrainSymbol";
                ws.Cell($"C{row.ToString()}").Value = "EventTrainLeadUnit";
                ws.Cell($"D{row.ToString()}").Value = "EventType";
                ws.Cell($"E{row.ToString()}").Value = "EventTrackGUID";
                ws.Cell($"F{row.ToString()}").Value = "EventTrackName";
                ws.Cell($"G{row.ToString()}").Value = "EventDirection";
                ws.Cell($"H{row.ToString()}").Value = "EventLocation";
                foreach (var movement in ListTrainEventMovements)
                {
                    if (ListTrains.Any(e => e.Symbol == movement.EventTrainSymbol))
                    {
                        row++;
                        ws.Cell($"A{row.ToString()}").Value = movement.EventTimeStamp;
                        ws.Cell($"B{row.ToString()}").Value = movement.EventTrainSymbol;
                        ws.Cell($"C{row.ToString()}").Value = movement.EventTrainLeadUnit;
                        ws.Cell($"D{row.ToString()}").Value = movement.EventType;
                        ws.Cell($"E{row.ToString()}").Value = movement.EventTrackGUID;
                        ws.Cell($"F{row.ToString()}").Value = movement.EventTrackName;
                        ws.Cell($"G{row.ToString()}").Value = movement.EventDirection;
                        ws.Cell($"H{row.ToString()}").Value = movement.EventLocation;
                    }
                }
                ws.Range(2, 1, row, 1).Style.NumberFormat.Format = "mm/DD/yyyy HH:mm:ss.0";
                ws.Columns("A", "BB").AdjustToContents();
                Console.WriteLine($"completed.");
            }
            catch (Exception ex)
            {
            }
        }
        private static void CreateTrainListWS(XLWorkbook wb)
        {
            row = 1;
            Console.WriteLine($"Building Train List ");
            try
            {
                var grp2080 = List2080.GroupBy(x => new { tID = x.Split('|')[3].Split('=')[1], lID = x.Split('|')[2].Split('=')[1] })
                    .Select(grp => grp.ToList()).ToList();
                //var list = grp2080[0].ToList();
                //add worksheet to workbook
                var ws = wb.Worksheets.Add("Trains");
                ws.SheetView.FreezeRows(1);
                ws.Cells("A1:BB1").Style.Font.Bold = true;
                ws.Cell($"A{row.ToString()}").Value = "Train Symbol";
                ws.Cell($"B{row.ToString()}").Value = "Engine ID";
                ws.Cell($"C{row.ToString()}").Value = "Call Time";
                ws.Cell($"D{row.ToString()}").Value = "Added?";
                ws.Cell($"E{row.ToString()}").Value = "Distance Traveled";
                ws.Cell($"F{row.ToString()}").Value = "ON STATION Movements?";
                ws.Cell($"G{row.ToString()}").Value = "2083?";
                ws.Cell($"H{row.ToString()}").Value = "2084?";
                ws.Cell($"I{row.ToString()}").Value = "Active?";
                ws.Cell($"J{row.ToString()}").Value = "Disengaged?";
                ws.Cell($"K{row.ToString()}").Value = "Cut-Out?";
                ws.Cell($"L{row.ToString()}").Value = "Failed?";
                ws.Cell($"M{row.ToString()}").Value = "Movements";
                ListTrains.OrderBy(x => x.CallTime);
                foreach (var train in ListTrains)
                {
                    AddToTrainWS(wb, train, "Train Sheet");
                }
                foreach (var item in DictTrainsAdd)
                {
                    tblTrains train = (new tblTrains { Symbol = item.Key.ToString().Split('|')[0], EngineID = item.Key.ToString().Split('|')[1], CallTime = DateTime.Parse(item.Value.ToString().Split('|')[0]) });
                    AddToTrainWS(wb, train, item.Value.Split('|')[1]);
                }
                ws.Range(2, 3, row, 3).Style.NumberFormat.Format = "mm/DD/yyyy HH:mm:ss.0";
                ws.Columns("A", "BB").AdjustToContents();
                ws.Range(2, 1, row, ws.Row(1).LastCellUsed().Address.ColumnNumber).Sort(3);
                Console.WriteLine($"Building Train List completed.");
            }
            catch (Exception ex)
            {
            }
        }
        private static void AddToTrainWS(XLWorkbook wb, tblTrains train, string addedVia)
        {
            var noTrainSheetlist = new List<tblTrainEventMovements>();
            if (addedVia == "OS EVENT" || addedVia == "2080")
            {
                noTrainSheetlist = ListTrainEventMovements.FindAll(e => e.EventTrainSymbol == train.Symbol && e.EventTrainLeadUnit == train.EngineID).ToList();
            }
            var ws = wb.Worksheet("Trains");
            int count = 0;
            count = ListTrainEventMovements.FindAll(e => e.TrainSheetUID == train.ActiveSubTrainSheet).Count;
            row++;
            ws.Cell($"A{row.ToString()}").Style.NumberFormat.Format = "@";
            ws.Cell($"A{row.ToString()}").Value = train.Symbol.ToString();
            ws.Cell($"B{row.ToString()}").Value = train.EngineID.ToString();
            ws.Cell($"C{row.ToString()}").Value = train.CallTime.ToString();
            ws.Cell($"D{row.ToString()}").Value = addedVia;
            ws.Cell($"E{row.ToString()}").Value = DistanceTraveled(train.Symbol, train.EngineID);
            if (count > 0)
            {
                ws.Cell($"F{row.ToString()}").Value = count;
            }
            else
            {
                ws.Cell($"F{row.ToString()}").Value = "NONE";
            }
            if (Dict2083Count.ContainsKey(train.Symbol))
            {
                ws.Cell($"G{row.ToString()}").Value = Dict2083Count[train.Symbol];
            }
            else
            {
                ws.Cell($"G{row.ToString()}").Value = "NONE";
            }
            if (Dict2084Count.ContainsKey(train.Symbol))
            {
                ws.Cell($"H{row.ToString()}").Value = Dict2084Count[train.Symbol];
            }
            else
            {
                ws.Cell($"H{row.ToString()}").Value = "NONE";
            }
            if (Dict2010.ContainsKey(train.EngineID))
            {
                int aCount = Regex.Matches(Dict2010[train.EngineID], "Active").Count;
                int dCount = Regex.Matches(Dict2010[train.EngineID], "Disengaged").Count;
                int cCount = Regex.Matches(Dict2010[train.EngineID], "Cut-Out").Count;
                int fCount = Regex.Matches(Dict2010[train.EngineID], "Failed").Count;
                if (aCount > 0)
                {
                    ws.Cell($"I{row.ToString()}").Value = Regex.Matches(Dict2010[train.EngineID], "Active").Count;
                }
                else
                {
                    ws.Cell($"I{row.ToString()}").Value = "NONE";
                }
                if (dCount > 0)
                {
                    ws.Cell($"J{row.ToString()}").Value = Regex.Matches(Dict2010[train.EngineID], "Disengaged").Count;
                }
                else
                {
                    ws.Cell($"J{row.ToString()}").Value = "NONE";
                }
                if (cCount > 0)
                {
                    ws.Cell($"K{row.ToString()}").Value = Regex.Matches(Dict2010[train.EngineID], "Cut-Out").Count;
                }
                else
                {
                    ws.Cell($"K{row.ToString()}").Value = "NONE";
                }
                if (fCount > 0)
                {
                    ws.Cell($"L{row.ToString()}").Value = Regex.Matches(Dict2010[train.EngineID], "Failed").Count;
                }
                else
                {
                    ws.Cell($"L{row.ToString()}").Value = "NONE";
                }
            }
            var trainUID = String.Empty;
            if (ListTrainUIDs.Any(x => x.ActiveSubTrainSheet == train.ActiveSubTrainSheet) && addedVia == "Train Sheet")
            {
                trainUID = ListTrainUIDs.FirstOrDefault(x => x.ActiveSubTrainSheet == train.ActiveSubTrainSheet).ActiveSubTrainSheet;
                if (DictTrainMovements.ContainsKey(trainUID))
                {
                    ws.Cell($"M{row.ToString()}").Value = DictTrainMovements[trainUID];
                }
                else
                {
                    ws.Cell($"M{row.ToString()}").Value = "NONE";
                }
            }
            else if (noTrainSheetlist.Count > 0)
            {
                var items = String.Empty;
                foreach (var item in noTrainSheetlist)
                {
                    if (items == string.Empty)
                    {
                        items = $"{item.EventTimeStamp}~{item.EventTrackGUID}~{item.EventTrackName}~{item.EventLocation.Replace("|", "~")}";
                    }
                    else
                    {
                        var oldValue = DictTrainMovements[item.TrainSheetUID];
                        items = $"{oldValue}|{item.EventTimeStamp}~{item.EventTrackGUID}~{item.EventTrackName}~{item.EventLocation.Replace("|", "~")}";
                    }
                }
                ws.Cell($"M{row.ToString()}").Value = items;
            }
            else
            {
                ws.Cell($"M{row.ToString()}").Value = "NONE";
            }
        }
        private static double DistanceTraveled(string trainSymbol, string engineID)
        {
            try
            {
                var list = List2080.FindAll(x => x.Contains(trainSymbol) && x.Contains(engineID));
                if (list.Count > 0)
                {
                    double totalDT = 0.00;
                    double prevMP = 0.00;
                    double currentDT = 0.00;
                    foreach (var line in list)
                    {
                        double mp = Convert.ToDouble(line.Split('|')[6].Split('=')[1]);
                        if (mp > 0)
                        {
                            if (prevMP != 0)
                            {
                                if (mp > prevMP)
                                {
                                    currentDT = mp - prevMP;
                                }
                                else
                                {
                                    currentDT = prevMP - mp;
                                }
                            }
                            else
                            {
                                currentDT = 0;
                            }
                            totalDT = totalDT + currentDT;
                            prevMP = mp;
                        }
                    }
                    return totalDT;
                }
                return 0.00;
            }
            catch (Exception ex)
            {
                return 0.00;
            }
        }
        private static double DistanceTraveled2080(string trainSymbol, string engineID, DateTime start, DateTime end)
        {
            try
            {
                var list = List2080.FindAll(x => x.Contains(trainSymbol) && x.Contains(engineID) && (DateTime.Parse(x.Split('|')[0]) >= start.AddSeconds(-5) && DateTime.Parse(x.Split('|')[0]) <= end.AddSeconds(5)));
                if (list.Count > 0)
                {
                    double totalDT = 0.00;
                    double prevMP = 0.00;
                    double currentDT = 0.00;
                    foreach (var line in list)
                    {
                        double mp = Convert.ToDouble(line.Split('|')[6].Split('=')[1]);
                        if (mp > 0)
                        {
                            if (prevMP != 0)
                            {
                                if (mp > prevMP)
                                {
                                    currentDT = mp - prevMP;
                                }
                                else
                                {
                                    currentDT = prevMP - mp;
                                }
                            }
                            else
                            {
                                currentDT = 0;
                            }
                            totalDT = totalDT + currentDT;
                            prevMP = mp;
                        }
                    }
                    return totalDT;
                }
                return 0.00;
            }
            catch (Exception ex)
            {
                return 0.00;
            }
        }
        private static void AddToAM(tblTrainEventMovements item, XLWorkbook wb)
        {
            try
            {
                var ws = wb.Worksheet($"{item.EventTrainLeadUnit.Split(' ')[0]} Trains");
                int row = 2;
                var test = item.EventTrainLeadUnit.Split(' ')[0];
                switch (item.EventTrainLeadUnit.Split(' ')[0])
                {
                    case "METX":
                        {
                            METXrow++;
                            row = METXrow;
                        }
                        break;
                    case "AMTK":
                        {
                            AMTKrow++;
                            row = AMTKrow;
                        }
                        break;
                }
                ws.Cell($"A{row.ToString()}").Value = item.EventTrainSymbol;
                ws.Cell($"A{row.ToString()}").Hyperlink = new XLHyperlink($"'{item.EventTrainSymbol}|{item.EventTrainLeadUnit}'!A1");
                ws.Cell($"B{row.ToString()}").Value = item.EventTrainLeadUnit;
                ws.Cell($"C{row.ToString()}").Value = item.EventTimeStamp;
                ws.Cell($"D{row.ToString()}").Value = item.EventDirection;
                ws.Cell($"E{row.ToString()}").Value = item.EventTrackGUID;
                ws.Cell($"F{row.ToString()}").Value = item.EventTrackName;
                ws.Cell($"G{row.ToString()}").Value = item.EventLocation.Split('|')[0];
                ws.Cell($"H{row.ToString()}").Value = item.EventLocation.Split('|')[1];
                ws.Columns("A", "H").AdjustToContents();
            }
            catch (Exception ex)
            {
            }
        }
        private static void CreateMETXAMTKWorksheets(XLWorkbook wb)
        {
            String[] AM = { "METX Trains", "AMTK Trains" };
            int pos = 1;
            foreach (var train in AM)
            {
                pos++;
                var ws = wb.Worksheets.Add(train);
                ws.SheetView.FreezeRows(1);
                wb.Worksheet(train).Position = pos;
                ws.Cells("A1:H1").Style.Font.Bold = true;
                ws.Cell($"A1").Value = "Symbol";
                ws.Cell($"B1").Value = "Engine ID";
                ws.Cell($"C1").Value = "OS Time";
                ws.Cell($"D1").Value = "Direction";
                ws.Cell($"E1").Value = "Track UID";
                ws.Cell($"F1").Value = "Track Name";
                ws.Cell($"G1").Value = "Control Point Name";
                ws.Cell($"H1").Value = "Control Point UID";
            }
            var grpEvents = ListTrainEventMovements.Where(x => x.EventTrainLeadUnit.Contains("AMTK") || x.EventTrainLeadUnit.Contains("METX")).ToList()
                .GroupBy(x => new { Symbol = x.EventTrainSymbol, EngineID = x.EventTrainLeadUnit })
                .Select(grp => grp.ToList()).ToList();
            grpEvents.OrderBy(grp => grp.ToList());
            foreach (var grp in grpEvents)
            {
                row = 1;
                var ws = wb.Worksheets.Add($"{grp[0].EventTrainSymbol}|{grp[0].EventTrainLeadUnit}");
                ws.SheetView.FreezeRows(1);
                ws.Cells("A1:H1").Style.Font.Bold = true;
                ws.Cell($"A{row.ToString()}").Value = "Symbol";
                ws.Cell($"A{row.ToString()}").Hyperlink = new XLHyperlink($"'{grp[0].EventTrainLeadUnit.Split(' ')[0]} Trains'!A1");
                ws.Cell($"B{row.ToString()}").Value = "Engine ID";
                ws.Cell($"C{row.ToString()}").Value = "OS Time";
                ws.Cell($"D{row.ToString()}").Value = "Direction";
                ws.Cell($"E{row.ToString()}").Value = "Track UID";
                ws.Cell($"F{row.ToString()}").Value = "Track Name";
                ws.Cell($"G{row.ToString()}").Value = "Control Point Name";
                ws.Cell($"H{row.ToString()}").Value = "Control Point UID";
                foreach (var item in grp)
                {
                    //row = 1;
                    Console.Write($"Building {item.EventTrainSymbol}|{item.EventTrainLeadUnit} Worksheet ");
                    try
                    {
                        row++;
                        int col = 0;
                        ws.Cell($"A{row.ToString()}").Value = item.EventTrainSymbol;
                        ws.Cell($"B{row.ToString()}").Value = item.EventTrainLeadUnit;
                        ws.Cell($"C{row.ToString()}").Value = item.EventTimeStamp;
                        ws.Cell($"C{row.ToString()}").Style.NumberFormat.Format = "mm/DD/yyyy HH:mm:ss.0";
                        ws.Cell($"D{row.ToString()}").Value = item.EventDirection;
                        ws.Cell($"E{row.ToString()}").Value = item.EventTrackGUID;
                        ws.Cell($"F{row.ToString()}").Value = item.EventTrackName;
                        ws.Cell($"G{row.ToString()}").Value = item.EventLocation.Split('|')[0];
                        ws.Cell($"H{row.ToString()}").Value = item.EventLocation.Split('|')[1];
                        ws.Columns("A", "H").AdjustToContents();
                        AddToAM(item, wb);
                    }
                    catch (Exception ex)
                    {
                    }
                }
                ws.Range(2, 1, row, 1).Style.NumberFormat.Format = "mm/DD/yyyy HH:mm:ss.0";
                ws.Columns("A", "E").AdjustToContents();
                Console.WriteLine($"completed.");
            }
        }
        private static void CreateTrainListWSNEW(XLWorkbook wb)
        {
            bool test = true;
            int row = 1;
            Console.WriteLine($"Building Train List ");
            try
            {
                var grpMovements = ListMoves.GroupBy(x => new { symbol = x.UID, engineID = x.SysData.Split('|').Last() })
                    .Select(grp => grp.ToList()).ToList();
                var ws = wb.Worksheets.Add("Trains - Eventlog DB");
                wb.Worksheet("Trains - Eventlog DB").Position = 1;
                ws.SheetView.FreezeRows(1);
                ws.Cells("A1:BB1").Style.Font.Bold = true;
                ws.Cell($"A{row.ToString()}").Value = "Train Symbol";
                ws.Cell($"B{row.ToString()}").Value = "Engine ID";
                ws.Cell($"C{row.ToString()}").Value = "Direction";
                ws.Cell($"D{row.ToString()}").Value = "Date/Time of first OS";
                ws.Cell($"E{row.ToString()}").Value = "Distance Traveled (2080)";
                ws.Cell($"F{row.ToString()}").Value = "Track Occupancies";
                ws.Cell($"G{row.ToString()}").Value = "2083?";
                ws.Cell($"H{row.ToString()}").Value = "2084?";
                ws.Cell($"I{row.ToString()}").Value = "Active?";
                ws.Cell($"J{row.ToString()}").Value = "Disengaged?";
                ws.Cell($"K{row.ToString()}").Value = "Cut-Out?";
                ws.Cell($"L{row.ToString()}").Value = "Failed?";
                ws.Cell($"M{row.ToString()}").Value = "Movements";
                ListTrains.OrderBy(x => x.CallTime);
                foreach (var grp in grpMovements)
                {
                    grp.OrderBy(x => x.EventTime);
                    //grp.OrderBy(x => x.EventTimeStamp);
                    var movements = String.Empty;
                    var movementsList = new List<string>();
                    var sameMove = true;
                    DateTime previousDT = DateTime.Parse("1/1/0001 12:00:00 AM");
                    foreach (var move in grp)
                    {
                        if (previousDT == DateTime.Parse("1/1/0001 12:00:00 AM"))
                        {
                            previousDT = move.EventTime;
                        }
                        else
                        {
                            TimeSpan ts = move.EventTime - previousDT;
                            if (ts.TotalMinutes > 90 /*|| (direction != previousDirection)*/)
                            {
                                sameMove = false;
                            }
                            else
                            {
                                sameMove = true;
                            }
                        }
                        if (sameMove)
                        {
                            if (movements == String.Empty)
                            {
                                movements = $"{move.EventTime.ToString("MM-dd-yyyy HH:mm:ss.fff")}~{move.SysData.Split('|')[1]}~{move.TrkName}";
                            }
                            else
                            {
                                movements = $"{movements}|{move.EventTime.ToString("MM-dd-yyyy HH:mm:ss.fff")}~{move.SysData.Split('|')[1]}~{move.TrkName}";
                            }
                        }
                        else
                        {
                            movementsList.Add(movements);
                            movements = $"{move.EventTime.ToString("MM-dd-yyyy HH:mm:ss.fff")}~{move.SysData.Split('|')[1]}~{move.TrkName}";
                        }
                        if (grp.IndexOf(move) == grp.Count - 1)
                        {
                            movementsList.Add(movements);
                        }
                        previousDT = move.EventTime;
                    }
                    foreach (var movement in movementsList)
                    {
                        var parts = movement.Split('|');
                        double distance = 0.00;
                        if (parts.GetUpperBound(0) > 1)
                        {
                            distance = DistanceTraveled2080(grp[0].SysData.Split('|')[3], grp[0].SysData.Split('|')[5], DateTime.Parse(movement.Split('|').First().Split('~')[0]), DateTime.Parse(movement.Split('|').Last().Split('~')[0]));
                        }
                        row++;
                        int col = 0;
                        ws.Cell($"A{row.ToString()}").Style.NumberFormat.Format = "@";
                        ws.Cell($"A{row.ToString()}").Value = grp[0].SysData.Split('|')[3];
                        ws.Cell($"B{row.ToString()}").Value = grp[0].SysData.Split('|')[5];
                        ws.Cell($"C{row.ToString()}").Value = grp[0].SysData.Split('|')[4];
                        ws.Cell($"D{row.ToString()}").Value = DateTime.Parse(movement.Split('~')[0]);
                        ws.Cell($"E{row.ToString()}").Value = distance;
                        ws.Cell($"F{row.ToString()}").Value = movement.Split('|').Count();
                        ws.Cell($"M{row.ToString()}").Value = movement;
                    }
                }
                ws.Range(2, 4, row, 4).Style.NumberFormat.Format = "mm/DD/yyyy HH:mm:ss.0";
                ws.Columns("A", "M").AdjustToContents();
                //ws.Range(2, 1, row, 13).Sort(2).Sort(1).Sort(4); //ws.Row(1).LastCellUsed().Address.ColumnNumber)
                Console.WriteLine($"Building Train List completed.");
            }
            catch (Exception ex)
            {
            }
        }
        private static void CreateTrainList(XLWorkbook wb)
        {
            try
            {
                Console.Write($"Building Train List");
                int row = 1;
                var ws = wb.Worksheets.Add("Trains");
                wb.Worksheet("Trains").Position = 1;
                ws.SheetView.FreezeRows(1);
                ws.Cells("A1:BB1").Style.Font.Bold = true;
                ws.Cell($"A{row.ToString()}").Value = "Train Symbol";
                ws.Cell($"B{row.ToString()}").Value = "Engine ID";
                ws.Cell($"C{row.ToString()}").Value = "Date/Time of first Movement";
                ws.Cell($"D{row.ToString()}").Value = "Distance Traveled";
                ws.Cell($"E{row.ToString()}").Value = "Track Occupancies";
                ws.Cell($"F{row.ToString()}").Value = "Movements";
                Console.WriteLine($" completed.");
                foreach (var move in ListSymbolMoves)
                {
                    List<string> tempList = new List<string>();
                    var parts = move.Value.Split('|');
                    var partsSorted = parts.OrderBy(x => DateTime.Parse(x.Split('~')[0].Split(' ')[0])).ThenBy(x => DateTime.Parse(x.Split('~')[0].Split(' ')[1])).ToList();
                    var movementsList = new List<string>();
                    var movements = string.Empty;
                    var sameMove = true;
                    var direction = string.Empty;
                    var previousDirection = string.Empty;
                    DateTime previousDT = DateTime.Parse("1/1/0001 12:00:00 AM");
                    var previousTrack = string.Empty;
                    var currentTrack = String.Empty;
                    var count = 0;
                    foreach (var move1 in partsSorted)
                    {
                        count++;
                        currentTrack = move1.Split('~')[1];
                        if (currentTrack == "151318")
                        {
                        }
                        if (!(move1 == partsSorted[0]))
                        {
                            if (ListTracks.FirstOrDefault(x => x.UID == int.Parse(previousTrack)).RightLimitMPRange > ListTracks.FirstOrDefault(x => x.UID == int.Parse(currentTrack)).LeftLimitMPRange)
                            {
                                direction = "increasing";
                                previousDirection = direction;
                            }
                            else if (ListTracks.FirstOrDefault(x => x.UID == int.Parse(previousTrack)).RightLimitMPRange < ListTracks.FirstOrDefault(x => x.UID == int.Parse(currentTrack)).LeftLimitMPRange)
                            {
                                direction = "decreasing";
                                previousDirection = direction;
                            }
                            if (direction != previousDirection)
                            {
                            }
                        }
                        if (move1.Split('~')[1] != previousTrack)
                        {
                            DateTime dt = DateTime.Parse(move1.Split('~')[0]);
                            
                            if (previousDT == DateTime.Parse("1/1/0001 12:00:00 AM"))
                            {
                                previousDT = dt;
                            }
                            else
                            {
                                TimeSpan ts = dt - previousDT;
                                if (ts.TotalMinutes > 90 || (direction != previousDirection))
                                {
                                    sameMove = false;
                                }
                                else
                                {
                                    sameMove = true;
                                }
                            }
                            if (sameMove)
                            {
                                if (movements == String.Empty)
                                {
                                    movements = $"{dt.ToString("MM-dd-yyyy HH:mm:ss.fff")}~{move1.Split('~')[1]}";
                                }
                                else
                                {
                                    if (!movements.Contains(move1.Split('~')[1]))
                                    {
                                        movements = $"{movements}|{dt.ToString("MM-dd-yyyy HH:mm:ss.fff")}~{move1.Split('~')[1]}";
                                    }
                                }
                            }
                            else
                            {
                                movementsList.Add(movements);
                                movements = $"{dt.ToString("MM-dd-yyyy HH:mm:ss.fff")}~{move1.Split('~')[1]}";
                            }
                            previousDT = dt;
                            previousTrack = move1.Split('~')[1];
                            previousDirection = direction;
                        }
                        if (move1 == partsSorted.Last())
                        {
                            movementsList.Add(movements);
                        }
                    }
                    foreach (var movement in movementsList)
                    {

                        var test = move.Key.Split('|');
                        row++;
                        ws.Cell($"A{row.ToString()}").Style.NumberFormat.Format = "@";
                        ws.Cell($"A{row.ToString()}").Value = move.Key.Split('|')[0]; /*grp[0].Symbol;*/
                        ws.Cell($"B{row.ToString()}").Value = move.Key.Split('|')[1]; /*grp[0].EngineID;*/
                        ws.Cell($"C{row.ToString()}").Value = DateTime.Parse(movement.Split('~')[0]);
                        ws.Cell($"D{row.ToString()}").Value = DistanceTraveled2080(move.Key.Split('|')[0], move.Key.Split('|')[1], DateTime.Parse(movement.Split('|').First().Split('~')[0]), DateTime.Parse(movement.Split('|').Last().Split('~')[0]));
                        ws.Cell($"E{row.ToString()}").Value = movement.Split('|').Count();
                        ws.Cell($"F{row.ToString()}").Value = movement;
                    }
                }
                ws.Range(2, 3, row, 3).Style.NumberFormat.Format = "mm/DD/yyyy HH:mm:ss.0";
                ws.Columns("A", "F").AdjustToContents();
                Console.WriteLine($" completed.");
            }
            catch (Exception ex)
            {
            }
        }
        private static void CreateTrainListNEW(XLWorkbook wb)
        {
            try
            {
                Console.Write($"Building Train List");
                int row = 1;
                var ws = wb.Worksheets.Add("Trains NEW");
                wb.Worksheet("Trains NEW").Position = 1;
                ws.SheetView.FreezeRows(1);
                ws.Cells("A1:BB1").Style.Font.Bold = true;
                ws.Cell($"A{row.ToString()}").Value = "Train Symbol";
                ws.Cell($"B{row.ToString()}").Value = "Engine ID";
                ws.Cell($"C{row.ToString()}").Value = "Date/Time of first Movement";
                ws.Cell($"D{row.ToString()}").Value = "Distance Traveled";
                ws.Cell($"E{row.ToString()}").Value = "Track Occupancies";
                ws.Cell($"F{row.ToString()}").Value = "Movements";
                Console.WriteLine($" completed.");
                foreach (var move in ListSymbolMoves)
                {
                    List<string> tempList = new List<string>();
                    var parts = move.Value.Split('|');
                    var partsSorted = parts.OrderBy(x => DateTime.Parse(x.Split('~')[0].Split(' ')[0])).ThenBy(x => DateTime.Parse(x.Split('~')[0].Split(' ')[1])).ToList();
                    var movementsList = new List<string>();
                    var movements = String.Empty;
                    var sameMove = true;
                    DateTime previousDT = DateTime.Parse("1/1/0001 12:00:00 AM");
                    var previousTrack = string.Empty;
                    var count = 0;
                    foreach (var move1 in partsSorted)
                    {
                        count++;
                        if (move1.Split('~')[1] != previousTrack)
                        {
                            DateTime dt = DateTime.Parse(move1.Substring(0, 23));
                            if (previousDT == DateTime.Parse("1/1/0001 12:00:00 AM"))
                            {
                                previousDT = dt;
                            }
                            else
                            {
                                TimeSpan ts = dt - previousDT;
                                if (ts.TotalMinutes > 90 /*|| (direction != previousDirection)*/)
                                {
                                    sameMove = false;
                                }
                                else
                                {
                                    sameMove = true;
                                }
                            }
                            if (sameMove)
                            {
                                if (movements == String.Empty)
                                {
                                    movements = $"{dt.ToString("MM-dd-yyyy HH:mm:ss.fff")}~{move.Value.Split('~')[1]}~{move1.Split('~')[1]}~{int.Parse(move1.Split('~')[1])}";
                                }
                                else
                                {
                                    movements = $"{movements}|{dt.ToString("MM-dd-yyyy HH:mm:ss.fff")}~{move1.Split('~')[1]}~{move1.Split('~')[1]}~{int.Parse(move1.Split('~')[1])}";
                                }
                            }
                            else
                            {
                                movementsList.Add(movements);
                                movements = $"{dt.ToString("MM-dd-yyyy HH:mm:ss.fff")}~{move1.Split('~')[1]}~{int.Parse(move1.Split('~')[1])}";
                            }
                            previousDT = dt;
                            previousTrack = move1.Split('~')[1];
                        }
                        if (move1 == partsSorted.Last())
                        {
                            movementsList.Add(movements);
                        }
                    }
                    foreach (var movement in movementsList)
                    {

                        var test = move.Key.Split('|');
                        row++;
                        ws.Cell($"A{row.ToString()}").Style.NumberFormat.Format = "@";
                        ws.Cell($"A{row.ToString()}").Value = move.Key.Split('|')[0];
                        ws.Cell($"B{row.ToString()}").Value = move.Key.Split('|')[1];
                        ws.Cell($"C{row.ToString()}").Value = DateTime.Parse(movement.Split('~')[0]);
                        ws.Cell($"D{row.ToString()}").Value = DistanceTraveledNEW(movement);
                        ws.Cell($"E{row.ToString()}").Value = movement.Split('|').Count();
                        ws.Cell($"F{row.ToString()}").Value = movement;
                    }
                }
                ws.Range(2, 3, row, 3).Style.NumberFormat.Format = "mm/DD/yyyy HH:mm:ss.0";
                ws.Columns("A", "F").AdjustToContents();
                Console.WriteLine($" completed.");
            }
            catch (Exception ex)
            {
            }
        }
        private static void CreateCADTrainList(XLWorkbook wb)
        {
            try
            {
                Console.Write($"Building CAD Train List");
                int row = 1;
                var ws = wb.Worksheets.Add("PTC List");
                wb.Worksheet("PTC List").Position = 1;
                ws.SheetView.FreezeRows(1);
                ws.Cells("A1:BB1").Style.Font.Bold = true;
                ws.Cell($"A{row.ToString()}").Value = "Train Symbol";
                ws.Cell($"B{row.ToString()}").Value = "Engine ID";
                ws.Cell($"C{row.ToString()}").Value = "Date/Time";
                ws.Cell($"D{row.ToString()}").Value = "PTC?";
                foreach (var train in ListOfTrains)
                {
                    int ptc = 0;
                    if (List2080.Any(x => x.Contains(train.Split('|')[0]) && x.Contains(train.Split('|')[1])))
                    {
                        ptc = 1;
                    }
                    row++;
                    ws.Cell($"A{row.ToString()}").Style.NumberFormat.Format = "@";
                    ws.Cell($"A{row.ToString()}").Value = train.Split('|')[0];
                    ws.Cell($"B{row.ToString()}").Value = train.Split('|')[1];
                    ws.Cell($"C{row.ToString()}").Value = train.Split('|')[2];
                    ws.Cell($"D{row.ToString()}").Value = ptc;
                }
                ws.Columns("A", "F").AdjustToContents();
                Console.WriteLine($" completed.");
            }
            catch (Exception ex)
            {
            }
        }
        private static void CreateTracksWithNoIndications(XLWorkbook wb)
        {
            try
            {
                Console.Write($"Building Tracks Occupancy List");
                int row = 1;
                var ws = wb.Worksheets.Add("Track Occupancy List");
                wb.Worksheet("Track Occupancy List").Position = 1;
                ws.SheetView.FreezeRows(1);
                ws.Cells("A1:BB1").Style.Font.Bold = true;
                ws.Cell($"A{row.ToString()}").Value = "Track UID";
                ws.Cell($"B{row.ToString()}").Value = "Name";
                ws.Cell($"C{row.ToString()}").Value = "Track Name";
                ws.Cell($"D{row.ToString()}").Value = "Track Name Alias";
                ws.Cell($"E{row.ToString()}").Value = "Occupancy?";
                foreach (var track in DictOfTrackOsIndications)
                {
                    var trackObject = ListTracks.First(x => x.UID == track.Key);
                    row++;
                    ws.Cell($"A{row.ToString()}").Style.NumberFormat.Format = "@";
                    ws.Cell($"A{row.ToString()}").Value = track.Key;
                    ws.Cell($"B{row.ToString()}").Value = trackObject.Name;
                    ws.Cell($"C{row.ToString()}").Value = trackObject.TrkName;
                    ws.Cell($"D{row.ToString()}").Value = trackObject.TrackNameAlias;
                    ws.Cell($"E{row.ToString()}").Value = track.Value;
                }
                ws.Columns("A", "F").AdjustToContents();
                Console.WriteLine($" completed.");
            }
            catch (Exception ex)
            {
            }
        }
        private static double DistanceTraveledNEW(string movement)
        {
            try
            {
                var moves = movement.Split('|');
                double totalDT = 0.00;
                var trackUIDprev = String.Empty;
                foreach (var move in moves)
                {
                    var trackUID = move.Split('~')[1];
                    if (trackUID != trackUIDprev)
                    {
                        var track = ListTracks.FirstOrDefault(x => x.UID == int.Parse(trackUID));
                        totalDT = totalDT + (track.RightLimitMPRange - track.LeftLimitMPRange);
                    }
                    trackUIDprev = trackUID;
                }
                return totalDT;
            }
            catch (Exception ex)
            {
                return 0.00;
            }
        }
        private static double GetRouteDistance(string movement)
        {
            try
            {
                var route = String.Empty;
                Dictionary<string, string> possibleRoute = new Dictionary<string, string>();
                var moves = movement.Split('|');
                double totalDT = 0.00;
                var trackUID = String.Empty;
                foreach (var move in moves)
                {
                    trackUID = move.Split('~')[1];
                    foreach (var rte in DictRoutes)
                    {
                        if (!possibleRoute.ContainsKey(rte.Key))
                        {
                            possibleRoute.Add(rte.Key, "false");
                        }
                        if (rte.Value.Contains(trackUID))
                        {
                            possibleRoute[rte.Key] = "true";
                        }
                        else
                        {
                            possibleRoute[rte.Key] = "false";
                        }
                    }
                }
                if (!possibleRoute.ContainsValue("true"))
                {
                    return 0.00;
                }
                else
                {
                    foreach (var move in DictRoutes[possibleRoute.First(x => x.Value == "true").Key].Split('|'))
                    {
                        trackUID = move.Split(':')[1];
                        var track = ListTracks.FirstOrDefault(x => x.UID == int.Parse(trackUID));
                        totalDT = totalDT + (track.RightLimitMPRange - track.LeftLimitMPRange);
                    }
                }
                return totalDT;
            }
            catch (Exception ex)
            {
                return 0.00;
            }
        }
    }
}
