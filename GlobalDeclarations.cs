using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOS_Logs
{
    public static class GlobalDeclarations
    {
        public static List<string> ListMessages = new List<string>();
        public static List<string> List2080 = new List<string>();
        public static List<tblTrainOsEvents> ListOSevents = new List<tblTrainOsEvents>();
        public static List<tblTrainEventMovements> ListTrainEventMovements = new List<tblTrainEventMovements>();
        public static List<tblTrains> ListTrains = new List<tblTrains>();
        public static List<tblTrainUID> ListTrainUIDs = new List<tblTrainUID>();
        public static List<string> ListOfTrainSymbols = new List<string>();
        public static Dictionary<string, int> Dict2083Count = new Dictionary<string, int>();
        public static Dictionary<string, int> Dict2084Count = new Dictionary<string, int>();
        public static Dictionary<string, string> Dict2010 = new Dictionary<string, string>();
        public static Dictionary<string, string> DictStateChanges = new Dictionary<string, string>();
        public static Dictionary<string, string> DictTrainMovements = new Dictionary<string, string>();
        public static Dictionary<string, string> DictTrackTrain = new Dictionary<string, string>();
        public static Dictionary<string, string> DictTrainSDED = new Dictionary<string, string>();
        public static List<string> ListTrainsDelete = new List<string>();
        public static Dictionary<string, string> DictTrainsAdd = new Dictionary<string, string>();
        public static Dictionary<string, DateTime> DictNewCallTimes = new Dictionary<string, DateTime>();
        public static Dictionary<string, string> DictEngineIDTimes = new Dictionary<string, string>();
        public static List<string> ListDBnames = new List<string>();
        public static List<tblTracks> ListTracks = new List<tblTracks>();
        public static List<tblMoves> ListMoves = new List<tblMoves>();
        public static List<TrainsCAD> ListTrainsCAD = new List<TrainsCAD>();
        public static Dictionary<string, string> ListSymbolMoves = new Dictionary<string, string>();
        public static List<TrainsCAD> ListMovesCombined = new List<TrainsCAD>();
        public static Dictionary<string, string> DictRoutes = new Dictionary<string, string>();
        public static List<string> ListOfTrains = new List<string>();
        public static Dictionary<int, bool> DictOfTrackOsIndications = new Dictionary<int, bool>();
    }
}
