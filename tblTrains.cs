using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOS_Logs
{
    public class tblTrainUID
    {
        public string TrainGUID { get; set; }
        public string ActiveSubTrainSheet { get; set; }
        public DateTime CallTime { get; set; }
    }

    public class tblTrains
    {
        public string Symbol { get; set; }

        public string EngineID { get; set; }

        public int? InUsed { get; set; }

        public string Subdivision { get; set; }

        public int? TerritoryAssignmentID { get; set; }

        public int? TrackGUID { get; set; }

        public string OsEvents { get; set; }

        public string SymbolComment { get; set; }

        public string Direction { get; set; }

        public DateTime? CallTime { get; set; }

        public string Units { get; set; }

        public string UnitComment { get; set; }

        public DateTime? DepTime { get; set; }

        public DateTime? ArrTime { get; set; }

        public DateTime? FinalTime { get; set; }

        public int? OLoads { get; set; }

        public int? OEmts { get; set; }

        public int? OTons { get; set; }

        public int? OLength { get; set; }

        public int? DLoads { get; set; }

        public int? DEmts { get; set; }

        public int? DTons { get; set; }

        public int? DLength { get; set; }

        public int? MaxSpd { get; set; }

        public int? HorsePowerTrailing { get; set; }

        public int? TonsPerOperativeBrake { get; set; }

        public bool? HazMat { get; set; }

        public bool? HighWide { get; set; }

        public string Origin { get; set; }

        public string Dest { get; set; }

        public string EOTD { get; set; }

        public string HOTD { get; set; }

        public string EngName { get; set; }

        public DateTime? EngOnDuty { get; set; }

        public DateTime? EngTimeUp { get; set; }

        public string CondName { get; set; }

        public DateTime? CondOnDuty { get; set; }

        public DateTime? CondTimeUp { get; set; }

        public string DelayInfo { get; set; }

        public string TrackBull { get; set; }

        public string BadOrderSetOuts { get; set; }

        public string EngNameRel { get; set; }

        public DateTime? EngOnDutyRel { get; set; }

        public DateTime? EngTimeUpRel { get; set; }

        public string CondNameRel { get; set; }

        public DateTime? CondOnDutyRel { get; set; }

        public DateTime? CondTimeUpRel { get; set; }

        public string TrainSheetEvents { get; set; }

        public string EngNameP { get; set; }

        public DateTime? EngOnDutyP { get; set; }

        public DateTime? EngTimeUpP { get; set; }

        public string CondNameP { get; set; }

        public DateTime? CondOnDutyP { get; set; }

        public DateTime? CondTimeUpP { get; set; }

        public string UnitsP { get; set; }

        public string EngineIDP { get; set; }

        public bool? Helper { get; set; }

        public bool? KeyTrain { get; set; }

        public string TrainGUID { get; set; }

        public int? TotalHP { get; set; }

        public string ScheduleStatus { get; set; }

        public string TrainLink { get; set; }

        public string TrailingHorsePower { get; set; }

        public int? StackOrder { get; set; }

        public string TrainType { get; set; }

        public bool? SpecialNote { get; set; }

        public string PseudoName { get; set; }

        public string SubType { get; set; }

        public bool? CBTCEquipped { get; set; }

        public int? TimeTableID { get; set; }

        public int? HomeRoadUID { get; set; }

        public int? CurrentGTBUID { get; set; }

        public bool? GTBActive { get; set; }

        public DateTime? GTBTimeIssued { get; set; }

        public string GTBStationID { get; set; }

        public string GTBStateID { get; set; }

        public string PTCStatus { get; set; }

        public string ActiveSubTrainSheet { get; set; }

        public string CondName2 { get; set; }

        public DateTime? CondOnDuty2 { get; set; }

        public DateTime? CondTimeUp2 { get; set; }

        public string CondName3 { get; set; }

        public DateTime? CondOnDuty3 { get; set; }

        public DateTime? CondTimeUp3 { get; set; }

        public string CrewCurrent { get; set; }

        public string CrewRelief { get; set; }

        public string CrewPending { get; set; }

        public string CrewLists { get; set; }

        public int? SecondaryInUsed { get; set; }

        public int? SecondarySubID { get; set; }

        public int? SecondaryTerrID { get; set; }

        public int? SecondaryTrackGUID { get; set; }

        public int? SecondaryStackOrder { get; set; }

        public int? SubdvnGeoReference { get; set; }

        public string TrainSheetRoute { get; set; }

        public int? LocomotiveTotalTonnage { get; set; }

        public string DPLocomotiveList { get; set; }

        public string MiscellanousInfoOne { get; set; }

        public string MiscellanousInfoTwo { get; set; }

        public string MiscellanousInfoThree { get; set; }

        public string HomeRoadCode { get; set; }

        public bool? FlashSymbol { get; set; }

        public bool? AutomaticModeOn { get; set; }

        public string PriorityCode { get; set; }

        public bool? PullerTrain { get; set; }

        public string TrainLegRoutePointGUIDs { get; set; }

        public string TrainLegConsistGUIDs { get; set; }

        public string TrainLegCrewGUIDs { get; set; }

        public string TrainLegLocoGUIDs { get; set; }

        public string TrainLegID { get; set; }

        public string TrainLegName { get; set; }

        public string TrainLegStatus { get; set; }

        public bool? GhostTrain { get; set; }

        public string TGBOLimits { get; set; }

        public string AuthorityDesignation { get; set; }

        public string CurrentConsistData { get; set; }

        public string LocoConsistData { get; set; }

        public string EventTimesdata { get; set; }

        public string TrainlegNotes { get; set; }

        public string TGBONumber { get; set; }

        public string TGBOStatus { get; set; }

        public string CTCAuthorityDesignation { get; set; }

        public string LocationInformation { get; set; }

        public string FuelInformation { get; set; }

        public string ConversationNotes { get; set; }

        public string EntityUID { get; set; }

        public int? NumberOperatingBrakes { get; set; }

        public int? NumberOfAxles { get; set; }

        public string CrewRelease { get; set; }

        public long? PTCAggregateCRC { get; set; }

        public bool? TrainIsNotActive { get; set; }

        public DateTime? SchDepTime { get; set; }

        public DateTime? SchArrTime { get; set; }

        public string TrainConsistInformationForPTC { get; set; }

        public string LocomotiveConsistInformationForPTC { get; set; }

        public string LocomotiveConsistsInformationMIS { get; set; }

        public int? GrossTons { get; set; }

        public int? GrossLength { get; set; }

        public DateTime? HOS { get; set; }

        public int? TotalCars { get; set; }

        public string VIA { get; set; }

        public DateTime? Release { get; set; }

        public bool? TestTrainPTC { get; set; }

        public string LocomotiveMISInformation { get; set; }

        public string TrainSymbolLink { get; set; }

        public string CrewCodeUID { get; set; }

        public string ConsistCodeUID { get; set; }

        public string BulletinRouteCode { get; set; }

        public string ComplianceStatus { get; set; }

        public string StagingOrigin { get; set; }

        public string StagingDest { get; set; }

        public string ScheduleType { get; set; }

        public bool? isPublic { get; set; }

        public DateTime? VATPTime { get; set; }

        public bool? Annulled { get; set; }

        public string TOPSImport { get; set; }

        public bool? SuperTrain { get; set; }

    }
}
