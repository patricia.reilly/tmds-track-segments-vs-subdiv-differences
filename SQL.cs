using FastMember;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BOS_Logs.GlobalDeclarations;

namespace BOS_Logs
{
    public static class SQL
    {
        public static SqlConnection GetSqlConnection(/*string DB*/)
        {
            string connectionString = String.Empty;
            var dbhost = ConfigurationSetter.DBHost;
            var dbname = ConfigurationSetter.DBName;
            var dbtimeout = ConfigurationSetter.DBTimeout;
            var dbauthentication = ConfigurationSetter.DBAuthentication;
            var lbt = 15;
            connectionString = $"server=tcp:{dbhost};{dbauthentication};Connection Timeout={dbtimeout};Load Balance Timeout={lbt}";
            SqlConnection _con = new SqlConnection(connectionString);
            return _con;
        }
        public static void CheckDBconnection(SqlConnection _con)
        {
            try
            {
                if (_con.State == ConnectionState.Broken)
                {
                    _con.Close();
                }
                if (_con.State == ConnectionState.Closed)
                {
                    _con.Open();
                }
            }
            catch (Exception ex)
            {
            }
        }
        public static void BuildOSEventsList()
        {
            String[] lists = { "tblTrainOsEvents", "tblTrainEventMovements", "tblTrainUID" };
            foreach (string table in lists)
            {
                var _Query = String.Empty;
                switch (table)
                {
                    case "tblTrainOsEvents":
                        {
                            _Query = $"SELECT * FROM (SELECT * FROM tmdsDatabaseDynamic.dbo.tblTrainOsEventsActive UNION ALL SELECT * FROM tmdsDatabaseDynamic.dbo.tblTrainOsEventsInActive) as l WHERE MONTH(OsTime) = {ConfigurationSetter.BOS_filter_month} AND YEAR(OsTime) = {ConfigurationSetter.BOS_filter_year} ORDER BY OsTime";
                        }
                        break;
                    case "tblTrainEventMovements":
                        {
                            _Query = $"SELECT * FROM (SELECT * FROM tmdsDatabaseDynamic.dbo.tblTrainEventActiveMovements UNION ALL SELECT * FROM tmdsDatabaseDynamic.dbo.tblTrainEventInActiveMovements) as l WHERE MONTH(EventTimeStamp) = {ConfigurationSetter.BOS_filter_month} AND YEAR(EventTimeStamp) = {ConfigurationSetter.BOS_filter_year} AND EventType = 'ON STATION'  AND (EventLocation LIKE '%150999' OR EventLocation LIKE '%151007' OR EventTrackGUID IN ('151133', '151117')) ORDER BY EventTimeStamp ASC";
                        }
                        break;
                    case "tblTrainUID":
                        {
                            _Query = $"SELECT TrainGUID, ActiveSubTrainSheet, CallTime FROM (SELECT TrainGUID, ActiveSubTrainSheet, CallTime FROM tmdsDatabaseDynamic.dbo.tblTrainsActive UNION ALL SELECT TrainGUID, ActiveSubTrainSheet, CallTime  FROM tmdsDatabaseDynamic.dbo.tblTrainsInActive) as l WHERE MONTH(CallTime) = {ConfigurationSetter.BOS_filter_month} AND YEAR(CallTime) = {ConfigurationSetter.BOS_filter_year} UNION ALL SELECT TrainGUID, ActiveSubTrainSheet, CallTime  FROM tmdsDatabaseDynamic.dbo.tblTrainsActive WHERE Symbol IN ('SW802','SW803','SW806','SW810','SW812','SW815','SW821','SW823','SW824','SW829') ORDER BY CallTime";
                        }
                        break;
                }
                DataReader(table, _Query);
            }
        }
        public static void UpdateTrain()
        {
            foreach (var train in ListTrains.FindAll(x => x.Symbol.StartsWith("SW8")).ToList())
            {
                var newCallTime = ListTrainEventMovements.FirstOrDefault(x => x.EventTrainSymbol.Equals(train.Symbol)).EventTimeStamp;
                if (!DictNewCallTimes.ContainsKey(train.ActiveSubTrainSheet))
                {
                    DictNewCallTimes.Add(train.ActiveSubTrainSheet, newCallTime);
                }
            }
            foreach (var train in DictNewCallTimes)
            {
                ListTrainUIDs.FirstOrDefault(x => x.ActiveSubTrainSheet == train.Key).CallTime = train.Value;
                ListTrains.FirstOrDefault(x => x.ActiveSubTrainSheet == train.Key).CallTime = train.Value;
            }
            var listTrains = ListTrains.FindAll(x => x.Symbol.StartsWith("SW8")).ToList();
        }
        public static void BuildTrainList()
        {
            var _Query = $"SELECT * FROM (SELECT * FROM tmdsDatabaseDynamic.dbo.tblTrainsActive UNION ALL SELECT * FROM tmdsDatabaseDynamic.dbo.tblTrainsInActive) as l WHERE CallTime >= '2020-12-31 17:00:00' AND CallTime < '2021-02-01 00:00:00' UNION ALL SELECT * FROM tmdsDatabaseDynamic.dbo.tblTrainsActive WHERE Symbol IN ('SW802','SW803','SW806','SW810','SW812','SW815','SW821','SW823','SW824','SW829') ORDER BY CallTime";
            DataReader("tblTrains", _Query);
            ListTrains.OrderBy(x => x.CallTime);
        }
        public static void GetTracks()
        {
            var _Query = $"SELECT UID, Name, UPPER(TrackNameAlias) AS TrackNameAlias, LeftLimitMPRange, RightLimitMPRange, TrkName FROM tmdsDatabaseStatic.dbo.tblCompTracks WHERE (ControlPoint IN('150999', '151007') AND UID NOT IN('151293','151593','151364','151360','151351','151352','151355','151522','151592','151807','151808'))";
            DataReader("tblTracks", _Query);
            ListTracks.OrderBy(x => x.UID);
        }

        public static void DataReader(string table, string _Query)
        {
            try
            {
                Console.Write($"Building {table} list");
                using (var _con = GetSqlConnection())
                {
                    using (SqlCommand cmd = new SqlCommand(_Query, _con))
                    {
                        CheckDBconnection(_con);
                        SqlDataReader dr = cmd.ExecuteReader();
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                switch (table)
                                {
                                    case "tblTracks":
                                        {
                                            var newObject = new tblTracks();
                                            MapDataToObject(dr, newObject);
                                            ListTracks.Add(newObject);
                                            if (!DictOfTrackOsIndications.ContainsKey(newObject.UID))
                                            {
                                                DictOfTrackOsIndications.Add(newObject.UID, false);
                                            }
                                        }
                                        break;
                                    case "tblMoves":
                                        {
                                            if (ListTracks.Any(x => x.UID == int.Parse(dr.GetString(3).Split('|')[1])))
                                            {
                                                var newObject = new tblMoves();
                                                MapDataToObject(dr, newObject);
                                                newObject.TrkName = ListTracks.FirstOrDefault(x => x.UID == int.Parse(newObject.SysData.Split('|')[1])).TrkName;
                                                ListMoves.Add(newObject);
                                                if (!ListSymbolMoves.ContainsKey($"{newObject.UID}|{newObject.SysData.Split('|').Last()}"))
                                                {
                                                    ListSymbolMoves.Add($"{newObject.UID}|{newObject.SysData.Split('|').Last()}", $"{newObject.EventTime.ToString("MM-dd-yyyy HH:mm:ss.fff")}~{int.Parse(newObject.SysData.Split('|')[1])}");
                                                }
                                                else
                                                {
                                                    if (!ListSymbolMoves.ContainsValue($"{newObject.EventTime.ToString("MM-dd-yyyy HH:mm:ss.fff")}~{newObject.SysData.Split('|')[1]}"))
                                                    {
                                                        var oldValue = ListSymbolMoves[$"{newObject.UID}|{newObject.SysData.Split('|').Last()}"];
                                                        ListSymbolMoves[$"{newObject.UID}|{newObject.SysData.Split('|').Last()}"] = $"{oldValue}|{newObject.EventTime.ToString("MM-dd-yyyy HH:mm:ss.fff")}~{newObject.SysData.Split('|')[1]}";
                                                    }
                                                }
                                                if (DictOfTrackOsIndications.ContainsKey(int.Parse(newObject.SysData.Split('|')[1])))
                                                {
                                                    DictOfTrackOsIndications[int.Parse(newObject.SysData.Split('|')[1])] = true;
                                                }
                                            }
                                        }
                                        break;
                                    case "tblTrains":
                                        {
                                            var newObject = new tblTrains();
                                            MapDataToObject(dr, newObject);
                                            ListTrains.Add(newObject);
                                        }
                                        break;
                                    case "tblTrainOsEvents":
                                        {
                                            var newObject = new tblTrainOsEvents();
                                            MapDataToObject(dr, newObject);
                                            ListOSevents.Add(newObject);
                                            if (DictOfTrackOsIndications.ContainsKey(newObject.TrackGUID))
                                            {
                                                DictOfTrackOsIndications[newObject.TrackGUID] = true;
                                            }
                                        }
                                        break;
                                    case "tblTrainEventMovements":
                                        {
                                            var newObject = new tblTrainEventMovements();
                                            MapDataToObject(dr, newObject);
                                            ListTrainEventMovements.Add(newObject);
                                            if (DictTrainMovements.ContainsKey(newObject.TrainSheetUID))
                                            {
                                                var oldValue = DictTrainMovements[newObject.TrainSheetUID];
                                                DictTrainMovements[newObject.TrainSheetUID] = $"{oldValue}|{newObject.EventTimeStamp}~{newObject.EventDirection}~{newObject.EventTrackGUID}~{newObject.EventTrackName}~{newObject.EventLocation.Replace("|", "~")}";
                                            }
                                            else
                                            {
                                                DictTrainMovements.Add(newObject.TrainSheetUID, $"{newObject.EventTimeStamp}~{newObject.EventDirection}~{newObject.EventTrackGUID}~{newObject.EventTrackName}~{newObject.EventLocation.Replace("|", "~")}");
                                            }
                                            tblTrains train = ListTrains.FirstOrDefault(x => x.Symbol == newObject.EventTrainSymbol && x.EngineID == newObject.EventTrainLeadUnit);
                                            if (train == null)
                                            {
                                                if (!(DictTrainsAdd.ContainsKey($"{newObject.EventTrainSymbol}|{newObject.EventTrainLeadUnit}")))
                                                {
                                                    DictTrainsAdd.Add($"{newObject.EventTrainSymbol}|{newObject.EventTrainLeadUnit}", $"{newObject.EventTimeStamp}|OS EVENT");
                                                }
                                            }
                                            if (DictOfTrackOsIndications.ContainsKey(newObject.EventTrackGUID))
                                            {
                                                DictOfTrackOsIndications[newObject.EventTrackGUID] = true;
                                            }
                                        }
                                        break;
                                    case "tblTrainUID":
                                        {
                                            var newObject = new tblTrainUID();
                                            MapDataToObject(dr, newObject);
                                            ListTrainUIDs.Add(newObject);
                                        }
                                        break;
                                }
                            }
                            Console.WriteLine($" completed.");
                            if (table == "tblTrainEventMovements")
                            {
                                var grpMovements = ListTrainEventMovements.GroupBy(x => new { symbol = x.EventTrainSymbol, engineID = x.EventTrainLeadUnit, direction = x.EventDirection })
                                                                          .Select(grp => grp.ToList()).ToList();
                            }
                        }
                        dr.Close();
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        public static void MapDataToObject<T>(this SqlDataReader dataReader, T newObject)
        {
            try
            {
                if (newObject == null) throw new ArgumentNullException(nameof(newObject));

                // Fast Member Usage
                var objectMemberAccessor = TypeAccessor.Create(newObject.GetType());
                var propertiesHashSet =
                        objectMemberAccessor
                        .GetMembers()
                        .Select(mp => mp.Name)
                        .ToHashSet(StringComparer.InvariantCultureIgnoreCase);
                for (int i = 0; i < dataReader.FieldCount; i++)
                {
                    try
                    {

                        var name = propertiesHashSet.FirstOrDefault(a => a.Equals(dataReader.GetName(i), StringComparison.InvariantCultureIgnoreCase));
                        if (!String.IsNullOrEmpty(name))
                        {
                            objectMemberAccessor[newObject, name]
                                = dataReader.IsDBNull(i) ? null : dataReader.GetValue(i);
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        public static void DatabaseLists()
        {
            try
            {
                using (var _con = GetSqlConnection())
                {
                    CheckDBconnection(_con);
                    using (SqlCommand cmd = new SqlCommand("SELECT name from sys.databases", _con))
                    using (SqlDataReader dr = cmd.ExecuteReader())
                    {
                        while (dr.Read())
                        {
                            var test = dr[0].ToString();
                            if (dr[0].ToString().Contains($"_{ConfigurationSetter.BOS_filter_month}") && dr[0].ToString().Contains(ConfigurationSetter.BOS_filter_year))
                            {
                                ListDBnames.Add(dr[0].ToString());
                            }
                        }
                    }
                }
                ListDBnames.Sort();
            }
            catch (Exception ex)
            {
            }
        }
        public static void GetMoves()
        {
            try
            {
                foreach (var db in ListDBnames)
                {
                    var _Query = $"SELECT EventTime, UID, Event, SysData FROM {db}.dbo.tblSystemEvents WHERE Event = 'MOVE'";
                    DataReader("tblMoves", _Query);
                }
                var grpMovements = ListMoves.GroupBy(x => new { symbol = x.UID, engineID = x.SysData.Split('|').Last() })
                    .Select(grp => grp.ToList()).ToList();
            }
            catch (Exception ex)
            {
            }
        }
        public static List<DateTime> GetDates(int year, int month)
        {
            return Enumerable.Range(1, DateTime.DaysInMonth(year, month))
                             .Select(day => new DateTime(year, month, day))
                             .ToList();
        }
    }
}
