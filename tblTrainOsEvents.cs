using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOS_Logs
{
    public class tblTrainOsEvents
    {
        public string TrainSheetGUID { get; set; }

        public string OsPoint { get; set; }

        public DateTime OsTime { get; set; }

        public int TrackGUID { get; set; }

        public string SubName { get; set; }

        public string ReportData { get; set; }

        public string RecordGUID { get; set; }

        public string Direction { get; set; }

        public string EventCreator { get; set; }

    }
}