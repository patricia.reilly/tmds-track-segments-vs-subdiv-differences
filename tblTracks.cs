using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOS_Logs
{
    public class tblTracks
    {
        public int UID { get; set; }
        public string Name { get; set; }
        public string TrackNameAlias { get; set; }
        public double LeftLimitMPRange { get; set; }
        public double RightLimitMPRange { get; set; }
        public string TrkName { get; set; }
    }
}
