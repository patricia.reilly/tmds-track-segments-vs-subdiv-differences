using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOS_Logs
{
    public class TrainsCAD
    {
        public string Symbol { get; set; }
        public string EngineID { get; set; }
        public DateTime TrkDateTime { get; set; }
        public int TrkUID { get; set; }
        public string TrkName { get; set; }
        public TrainsCAD(string symbol, string locoID, DateTime dt, int trnTrack, string trnTrackName)
        {
            Symbol = symbol;
            this.EngineID = locoID;
            this.TrkDateTime = dt;
            this.TrkUID = trnTrack;
            this.TrkName = trnTrackName;
        }
    }
}
