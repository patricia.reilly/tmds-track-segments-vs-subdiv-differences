using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BOS_Logs.GlobalDeclarations;

namespace BOS_Logs
{
    public class ConfigurationSetter
    {
        private static volatile object SyncRoot = new object();
        private static ConfigurationSetter instance;
        public static string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        public static string configFileName = Path.Combine(path, "config.json");

        private ConfigurationSetter()
        {
            SetConfiguration(null, null);
        }
        public static void SetConfiguration(object obj, EventArgs ea)
        {
            if (File.Exists(configFileName))
            {
                using (StreamReader streamReader = new StreamReader(configFileName))
                {
                    dynamic data = JsonConvert.DeserializeObject<dynamic>(streamReader.ReadToEnd());
                    SetApplicationParams(data.Application);
                    SetDatabaseParameters(data.SQLServer);
                    SetRoutes(data.Routes);
                }
            }
        }
        #region SetApplicationParams
        public static string Railroad { get; set; }
        public static string[] BOS_Log_Location { get; set; }
        public static string BOS_Log_Name { get; set; }
        public static string BOS_filter_year { get; set; }
        public static string BOS_filter_month { get; set; }
        public static string CAD_Log_Location { get; set; }
        public static string CAD_filter_year { get; set; }
        public static string CAD_filter_month { get; set; }
        public static bool OpenReportUponCompletion { get; set; }

        private static void SetApplicationParams(dynamic data)
        {
            try
            {
                Railroad = data.Railroad;
                List<string> logLocs = new List<string>();
                var locationStrings = data.BOS_Log_Location;
                foreach (var str in locationStrings)
                {
                    logLocs.Add(str.ToString());
                }
                BOS_Log_Location = logLocs.ToArray();
                BOS_Log_Name = data.Log_Name;
                BOS_filter_year = data.BOS_filter_year;
                BOS_filter_month = data.BOS_filter_month;
                CAD_filter_year = data.CAD_filter_year;
                CAD_filter_month = data.CAD_filter_month;
                CAD_Log_Location = data.CAD_Log_Location;
                OpenReportUponCompletion = data.OpenReportUponCompletion;
            }
            catch (Exception ex)
            {
            }
        }
        #endregion
        #region SetDatabaseParameters
        public static string DBHost { get; set; }
        public static string DBName { get; set; }
        public static string DBUser { get; set; }
        public static string DBPassword { get; set; }
        public static int DBTimeout { get; set; }
        public static string DBAuthentication { get; set; }
        public static string DBSchema { get; set; }

        private static void SetDatabaseParameters(dynamic data)
        {
            try
            {
                DBHost = data.DBHost;
                DBName = data.DBName;
                //DBUser = sqldb.DBUser;
                //DBPassword = DecryptString(data.DBPassword.ToString());
                DBTimeout = data.DBTimeout;
                DBAuthentication = data.DBAuthentication;
                if (DBName == "PTC_CEC")
                {
                    DBSchema = "cec.";
                }
                else
                {
                    DBSchema = "";
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion
        #region SetRoutes
        public static string[] Routes { get; set; }

        private static void SetRoutes(dynamic data)
        {
            try
            {
                List<string> routes = new List<string>();
                var routeStrings = data;
                foreach (var str in routeStrings)
                {
                    routes.Add(str.ToString());
                    DictRoutes.Add(str.ToString().Split('~')[0], str.ToString().Split('~')[1]);
                }
            }
            catch (Exception ex)
            {
            }
        }
        #endregion
    }
}