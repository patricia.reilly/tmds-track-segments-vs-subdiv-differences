using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOS_Logs
{
    public class tblTrainEventMovements
    {
        public string TrainSheetUID { get; set; }

        public string EventType { get; set; }

        public DateTime EventTimeStamp { get; set; }

        public string EventTimeZone { get; set; }

        public int EventTrackGUID { get; set; }

        public string EventTrackName { get; set; }

        public decimal? EventDistance { get; set; }

        public string EventDirection { get; set; }

        public string EventLocation { get; set; }

        public string EventTrainSymbol { get; set; }

        public string EventTrainLeadUnit { get; set; }

        public string EventTrainHomeRoadCode { get; set; }

        public string EventTrainLocomotiveData { get; set; }

        public string EventTrainConsistData { get; set; }

        public string EventTrainType { get; set; }

        public string UID { get; set; }

        public int? EventSubUID { get; set; }

        public int? EventTerritoryID { get; set; }

    }
}
