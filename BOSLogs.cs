using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BOS_Logs.GlobalDeclarations;

namespace BOS_Logs
{
    public static class BOSLogs
    {
        public static void ReadBOSLogs()
        {
            try
            {
                foreach (var loc in ConfigurationSetter.BOS_Log_Location)
                {
                    string line = string.Empty;
                    if (Directory.Exists(loc))
                    {
                        DirectoryInfo dirSource = new DirectoryInfo(loc);
                        var allLogArchives = dirSource.GetFiles("*.zip", SearchOption.AllDirectories).Where(x => x.Name.Contains($"-{ConfigurationSetter.BOS_filter_year}{ConfigurationSetter.BOS_filter_month}")).ToList();
                        var count = allLogArchives.Count;
                        var processed = 0;
                        var lineCount = 0;
                        var prevLine = String.Empty;
                        var prevMSG2010 = String.Empty;
                        var prevMSG2003 = String.Empty;
                        var source = String.Empty;
                        var tID = String.Empty;
                        var sebt = String.Empty;
                        var sebhemp = String.Empty;
                        var sebhes = String.Empty;
                        var sebhetn = String.Empty;
                        var sebdot = String.Empty;
                        var sebts = String.Empty;
                        var at = String.Empty;
                        var items2084 = new string[]
                                                {
                                                    "|<TRAIN ID=",
                                                    "|<START EMERGENCY BRAKING TIME=",
                                                    "|<START EMERGENCY BRAKING HEAD END MILEPOST=",
                                                    "|<START EMERGENCY BRAKING HEAD END TRACK NAME=",
                                                    "|<START EMERGENCY BRAKING HEAD END SUBDIVISION/DISTRICT ID=",
                                                    "|<START EMERGENCY BRAKING DIRECTION OF TRAVEL=",
                                                    "|<START EMERGENCY BRAKING TRAIN SPEED (MPH)=",
                                                    "|<APPLICATION TYPE=",
                                                };
                        foreach (var archive in allLogArchives)
                        {
                            processed++;
                            Console.WriteLine($"Processing {archive.Name} | File #{processed} / {count}");
                            using (ZipArchive zip = ZipFile.Open(archive.FullName, ZipArchiveMode.Read))
                            {
                                foreach (ZipArchiveEntry entry in zip.Entries)
                                {
                                    using (StreamReader sr = new StreamReader(entry.Open()))
                                    {
                                        while ((line = sr.ReadLine()) != null)
                                        {
                                            lineCount++;
                                            if (line.Contains("Locomotive Position Report (02080):") && !line.Contains("Non-Controlling") && line.Contains("HeadEndSCAC=BRC"))
                                            {
                                                var parts = line.Split('|');
                                                var msgDate = parts[0].Split(' ')[0];
                                                var msgTime = parts[0].Split(' ')[1];
                                                var msgDateTime = DateTime.Parse(parts[0]).ToString("MM-dd-yyyy HH:mm:ss.fff");
                                                var msg = parts[4].Remove(0, 41)
                                                    .Replace("> <", "|")
                                                    .Replace("<", "")
                                                    .Replace(">", "");
                                                double mp = Convert.ToDouble(msg.Split('|')[4].Split('=')[1]);
                                                var trackAlias = msg.Split('|')[7].Split('=')[1];
                                                var subID = msg.Split('|')[9].Split('=')[1];
                                                if ((mp == 0 || (mp >= 13 && mp <= 16.1471)) && msg.Split('|')[9].Split('=')[1] == "1")
                                                {
                                                    ListMessages.Add($"{parts[0].ToString()}|{2080}|{msg}");
                                                    List2080.Add($"{parts[0].ToString()}|{2080}|{msg}");
                                                    if (DictTrackTrain.ContainsKey(msg.Split('|')[0].Split('=')[1]))
                                                    {
                                                        DictTrackTrain[msg.Split('|')[0].Split('=')[1]] = "true";
                                                    }
                                                    else
                                                    {
                                                        DictTrackTrain.Add(msg.Split('|')[0].Split('=')[1], "true");
                                                    }
                                                    var trackUID = string.Empty;
                                                    // Add to ListSymbolMoves
                                                    if (ListTracks.Any(x => x.TrackNameAlias == trackAlias && (x.LeftLimitMPRange <= mp && x.RightLimitMPRange >= mp)))
                                                    {
                                                        var symbol = msg.Split('|')[1].Split('=')[1];
                                                        var locoID = msg.Split('|')[0].Split('=')[1];
                                                        var state = msg.Split('|')[22].Split('=')[1];
                                                        trackUID = ListTracks.First(x => x.TrackNameAlias == trackAlias && (x.LeftLimitMPRange <= mp && x.RightLimitMPRange >= mp)).UID.ToString();
                                                        if (!ListSymbolMoves.ContainsKey($"{symbol}|{locoID}"))
                                                        {
                                                            ListSymbolMoves.Add($"{symbol}|{locoID}", $"{msgDateTime}~{trackUID}");
                                                        }
                                                        else
                                                        {
                                                            if (!ListSymbolMoves.ContainsValue($"{parts[0]}~{trackUID}"))
                                                            {
                                                                var oldValue = ListSymbolMoves[$"{symbol}|{locoID}"];
                                                                ListSymbolMoves[$"{symbol}|{locoID}"] = $"{oldValue}|{msgDateTime}~{trackUID}";
                                                            }
                                                        }
                                                        if (DictOfTrackOsIndications.ContainsKey(int.Parse(trackUID)))
                                                        {
                                                            DictOfTrackOsIndications[int.Parse(trackUID)] = true;
                                                        }
                                                    }

                                                    tblTrains train = new tblTrains();
                                                    if (msg.Split('|')[0].Split('=')[1].ToString().Contains("METX"))
                                                    {
                                                        train = ListTrains.FirstOrDefault(x => x.EngineID == msg.Split('|')[0].Split('=')[1].ToString());
                                                    }
                                                    else
                                                    {
                                                        train = ListTrains.FirstOrDefault(x => x.Symbol == msg.Split('|')[1].Split('=')[1].ToString() && x.EngineID == msg.Split('|')[0].Split('=')[1].ToString());
                                                    }
                                                    if (train == null)
                                                    {
                                                        if (!(DictTrainsAdd.ContainsKey($"{msg.Split('|')[1].Split('=')[1]}|{msg.Split('|')[0].Split('=')[1]}")))
                                                        {
                                                            DictTrainsAdd.Add($"{msg.Split('|')[1].Split('=')[1]}|{msg.Split('|')[0].Split('=')[1]}", $"{msgDate} {msgTime}|2080");
                                                        }
                                                    }
                                                    if (!(DictEngineIDTimes.ContainsKey($"{msg.Split('|')[0].Split('=')[1]}|{msg.Split('|')[1].Split('=')[1]}")))
                                                    {
                                                        DictEngineIDTimes.Add($"{msg.Split('|')[0].Split('=')[1]}|{msg.Split('|')[1].Split('=')[1]}", $"{msgDate} {msgTime}|");
                                                    }
                                                    else
                                                    {
                                                        var startDT = DictEngineIDTimes[$"{msg.Split('|')[0].Split('=')[1]}|{msg.Split('|')[1].Split('=')[1]}"].Split('|')[0];
                                                        DictEngineIDTimes[$"{msg.Split('|')[0].Split('=')[1]}|{msg.Split('|')[1].Split('=')[1]}"] = $"{startDT}|{msgDate} {msgTime}";
                                                    }
                                                }
                                                else
                                                {
                                                    if (DictTrackTrain.ContainsKey(msg.Split('|')[0].Split('=')[1]))
                                                    {
                                                        DictTrackTrain[msg.Split('|')[0].Split('=')[1]] = "false";
                                                    }
                                                }
                                                if (!ListOfTrainSymbols.Contains(msg.Split('|')[1].Split('=')[1]))
                                                {
                                                    ListOfTrainSymbols.Add(msg.Split('|')[1].Split('=')[1]);
                                                }
                                            }
                                            if (line.Contains(">>IETMS MESSAGE 2010:"))
                                            {
                                                var parts = line.Split('|');
                                                var msgDate = parts[0].Split(' ')[0];
                                                var msgTime = parts[0].Split(' ')[1];
                                                var msg = parts[4].Remove(0, 22)
                                                    .Replace("><", "|")
                                                    .Replace("> <", "|")
                                                    .Replace("<", "")
                                                    .Replace(">", "");
                                                prevMSG2010 = msg;
                                            }
                                            if (line.Contains(">>IETMS MESSAGE 2010-PROCESSED:"))
                                            {
                                                var parts = line.Split('|');
                                                var msgDate = parts[0].Split(' ')[0];
                                                var msgTime = parts[0].Split(' ')[1];
                                                var msg = parts[4].Remove(0, 32)
                                                    .Replace("><", "|")
                                                    .Replace("> <", "|")
                                                    .Replace("<", "")
                                                    .Replace(">", "");
                                                if (DictTrackTrain.ContainsKey(msg.Split('|')[1]))
                                                {
                                                    if (DictTrackTrain[msg.Split('|')[1]] == "true")
                                                    {
                                                        ListMessages.Add($"{parts[0]}|{2010}|LocoID={msg.Split('|')[1]}|{prevMSG2010}");
                                                        if (msg.Split('|')[1] != "")
                                                        {
                                                            if (Dict2010.ContainsKey(msg.Split('|')[1]))
                                                            {
                                                                var oldValue = Dict2010[msg.Split('|')[1]];
                                                                Dict2010[msg.Split('|')[1]] = $"{oldValue}|<{msgDate} {msgTime}|{prevMSG2010.Split('|')[1].Split('=')[1]}|{prevMSG2010.Split('|')[5].Split('=')[1]}>";
                                                            }
                                                            else
                                                            {
                                                                Dict2010.Add(msg.Split('|')[1], $"<{msgDate} {msgTime}|{prevMSG2010.Split('|')[1].Split('=')[1]}|{prevMSG2010.Split('|')[5].Split('=')[1]}>");
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            if (line.Contains(">>IETMS MESSAGE 2003:"))
                                            {
                                                var parts = line.Split('|');
                                                var msgDate = parts[0].Split(' ')[0];
                                                var msgTime = parts[0].Split(' ')[1];
                                                var msg = parts[4].Remove(0, 22)
                                                    .Replace("><", "|")
                                                    .Replace("> <", "|")
                                                    .Replace("<", "")
                                                    .Replace(">", "");
                                                prevMSG2003 = msg;
                                                if (!ListOfTrainSymbols.Contains(msg.Split('|')[3].Split('=')[1]))
                                                {
                                                    ListOfTrainSymbols.Add(msg.Split('|')[3].Split('=')[1]);
                                                }
                                            }
                                            if (line.Contains(">>IETMS MESSAGE 2003-PROCESSED:"))
                                            {
                                                var parts = line.Split('|');
                                                var msgDate = parts[0].Split(' ')[0];
                                                var msgTime = parts[0].Split(' ')[1];
                                                var msg = parts[4].Remove(0, 32)
                                                    .Replace("><", "|")
                                                    .Replace("> <", "|")
                                                    .Replace("<", "")
                                                    .Replace(">", "");
                                                if (DictTrackTrain.ContainsKey(msg.Split('|')[1]))
                                                {
                                                    if (DictTrackTrain[msg.Split('|')[1]] == "true")
                                                    {
                                                        ListMessages.Add($"{parts[0]}|{2003}|LocoID={msg.Split('|')[1]}|{prevMSG2003}");
                                                    }
                                                }
                                            }
                                            //VETMS-SOURCE:
                                            if (line.Contains("|VETMS-SOURCE:"))
                                            {
                                                var parts = line.Split('|');
                                                var msgDate = parts[0].Split(' ')[0];
                                                var msgTime = parts[0].Split(' ')[1];
                                                var msg = parts[4].Remove(0, 14)
                                                    .Replace(":itc", "");
                                                source = msg;
                                            }
                                            if (line.Contains("(02083):"))
                                            {
                                                var parts = line.Split('|');
                                                var msgDate = parts[0].Split(' ')[0];
                                                var msgTime = parts[0].Split(' ')[1];
                                                var msg = parts[4].Remove(0, 55)
                                                    .Replace("><", "|")
                                                    .Replace("> <", "|")
                                                    .Replace("<", "")
                                                    .Replace(">", "");
                                                var trainSymbol = msg.Split('|')[1].Split('=')[1];
                                                var rr = source.Split('.')[2].ToUpper().ToString();
                                                var trainNum = source.Split('.')[3].ToString();
                                                var trainID = String.Empty;
                                                int padding = 10 - (rr.Length + trainNum.Length);
                                                var spacing = " ";
                                                for (int spc = 1; spc < padding; spc++)
                                                {
                                                    spacing = $"{spacing} ";
                                                }
                                                if ((rr.Length + trainNum.Length) < 10)
                                                {
                                                    trainID = $"{rr}{spacing}{trainNum}";
                                                }
                                                if (DictTrackTrain.ContainsKey(trainID))
                                                {
                                                    if (DictTrackTrain[trainID] == "true")
                                                    {
                                                        ListMessages.Add($"{parts[0]}|{2083}|LocoID={trainID}|{msg}");
                                                        if (trainSymbol != "")
                                                        {
                                                            if (Dict2083Count.ContainsKey(trainSymbol))
                                                            {
                                                                Dict2083Count[trainSymbol]++;
                                                            }
                                                            else
                                                            {
                                                                Dict2083Count.Add(trainSymbol, 1);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                            //2084 items
                                            try
                                            {
                                                foreach (var item in items2084)
                                                {
                                                    if (line.Contains(item))
                                                    {
                                                        try
                                                        {
                                                            var parts = line.Split('|');
                                                            var msgDate = parts[0].Split(' ')[0];
                                                            var msgTime = parts[0].Split(' ')[1];
                                                            var msg = parts[4]
                                                                .Replace("><", "|")
                                                                .Replace("> <", "|")
                                                                .Replace("<", "")
                                                                .Replace(">", "");

                                                            switch (item)
                                                            {
                                                                case "|<TRAIN ID=":
                                                                    {
                                                                        if (msg.Split('=')[1].Contains(' '))
                                                                        {
                                                                            tID = msg.Split('=')[1].Substring(0, msg.Split('=')[1].IndexOf(" "));
                                                                        }
                                                                        else
                                                                        {
                                                                            tID = msg.Split('=')[1];
                                                                        }
                                                                        //if (tID == "Q37001")
                                                                        //{ }
                                                                    }
                                                                    break;
                                                                case "|<START EMERGENCY BRAKING TIME=":
                                                                    {
                                                                        sebt = msg.Split('=')[1];
                                                                    }
                                                                    break;
                                                                case "|<START EMERGENCY BRAKING HEAD END MILEPOST=":
                                                                    {
                                                                        sebhemp = msg.Split('=')[1];
                                                                    }
                                                                    break;
                                                                case "|<START EMERGENCY BRAKING HEAD END SUBDIVISION/DISTRICT ID=":
                                                                    {
                                                                        sebhes = msg.Split('=')[1];
                                                                    }
                                                                    break;
                                                                case "|<START EMERGENCY BRAKING HEAD END TRACK NAME=":
                                                                    {
                                                                        sebhetn = msg.Split('=')[1];
                                                                    }
                                                                    break;
                                                                case "|<START EMERGENCY BRAKING DIRECTION OF TRAVEL=":
                                                                    {
                                                                        sebdot = msg.Split('=')[1];
                                                                    }
                                                                    break;
                                                                case "|<START EMERGENCY BRAKING TRAIN SPEED (MPH)=":
                                                                    {
                                                                        sebts = msg.Split('=')[1];
                                                                    }
                                                                    break;
                                                                case "|<APPLICATION TYPE=":
                                                                    {
                                                                        at = msg.Split('=')[1];
                                                                    }
                                                                    break;
                                                            }
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                        }
                                                    }
                                                }
                                                try
                                                {
                                                    if (line.Contains(">>IETMS MESSAGE 2084-PROCESSED:"))
                                                    {
                                                        var parts = line.Split('|');
                                                        var msgDate = parts[0].Split(' ')[0];
                                                        var msgTime = parts[0].Split(' ')[1];
                                                        var msg = parts[4].Remove(0, 32)
                                                            .Replace("><", "|")
                                                            .Replace("> <", "|")
                                                            .Replace("<", "")
                                                            .Replace(">", "");
                                                        var test = msg.Split(':')[1].Trim();
                                                        if (DictTrackTrain.ContainsKey(msg.Split(':')[1].Trim()))
                                                        {
                                                            if (DictTrackTrain[msg.Split(':')[1].Trim()] == "true")
                                                            {
                                                                ListMessages.Add($"{parts[0]}|{2084}|LocoID={msg.Split(':')[1].Trim()}|TRAIN ID={tID}|START EMERGENCY BRAKING TIME={sebt}|START EMERGENCY BRAKING HEAD END MILEPOST={sebhemp}|START EMERGENCY BRAKING HEAD END SUBDIVISION={sebhes}|START EMERGENCY BRAKING HEAD END TRACK NAME={sebhetn}|START EMERGENCY BRAKING DIRECTION OF TRAVEL={sebdot}|START EMERGENCY BRAKING TRAIN SPEED (MPH)={sebts}|APPLICATION TYPE={at}");
                                                                if (tID != "")
                                                                {
                                                                    if (Dict2084Count.ContainsKey(tID))
                                                                    {
                                                                        Dict2084Count[tID]++;
                                                                    }
                                                                    else
                                                                    {
                                                                        Dict2084Count.Add(tID, 1);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                }
                                            }
                                            catch (Exception ex)
                                            {
                                            }
                                            prevLine = line;
                                        }
                                    }
                                }
                            }
                        }
                        Console.WriteLine($"Processed {lineCount} lines.");
                        //Console.WriteLine($"Processed {List2080.Count} 2080 messages.");
                    }
                }
                ListMessages.Sort();
                var activeCount = ListMessages.Where(x => x.Contains("=Active") && x.Contains("|2080|")).Count();
                var cutoutCount = ListMessages.Where(x => x.Contains("=Cut-Out") && x.Contains("|2080|")).Count();
                var disengagedCount = ListMessages.Where(x => x.Contains("=Disengaged") && x.Contains("|2080|")).Count();
                var failedCount = ListMessages.Where(x => x.Contains("=Failed") && x.Contains("|2080|")).Count();
                var failed = ListMessages.FirstOrDefault(x => x.Contains("=Failed") && x.Contains("|2080|"));
            }
            catch (Exception ex)
            {
            }
        }
    }
}