using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BOS_Logs.GlobalDeclarations;

namespace BOS_Logs
{
    public static class CADLogs
    {
        public static void ReadCADLogs()
        {
            try
            {
                string line = string.Empty;
                if (Directory.Exists(ConfigurationSetter.CAD_Log_Location))
                {
                    var cadLogs = new DirectoryInfo(ConfigurationSetter.CAD_Log_Location).GetFiles().Where(x => x.LastWriteTime.Date.Month == int.Parse(ConfigurationSetter.CAD_filter_month.Replace("-", "")) && x.LastWriteTime.Date.Year == int.Parse(ConfigurationSetter.CAD_filter_year.Replace("-", "20"))).OrderBy(f => f.LastWriteTime).ToList();
                    foreach (var log in cadLogs)
                    {
                        int lineNum = 0;
                        int count = 0;
                        Stopwatch stopwatch = new Stopwatch();
                        using (StreamReader sr = new StreamReader(log.FullName))
                        {
                            Console.Write($"Processing CAD log file: {log.Name}");
                            var dt = string.Empty;
                            string[] parts = { };
                            var state = string.Empty;
                            var symbol = string.Empty;
                            var locoID = string.Empty;
                            int me = 0;
                            int trnTrack = 0;
                            int thisTrack = 0;
                            int thisTrack1 = 0;
                            stopwatch.Start();
                            while ((line = sr.ReadLine()) != null)
                            {
                                lineNum++;
                                try
                                {
                                    if (line.Length > 22 && line.Substring(0, 1) != "\t")
                                    {
                                        DateTime temp;
                                        if (DateTime.TryParse(line.Substring(0, 23), out temp))
                                        {
                                            dt = line.Substring(0, 23);
                                        }
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                                try
                                {
                                    if (line != "" && !line.Contains("\t") && line.Substring(0, 3) == "ME:")
                                    {
                                        var test = line.Split(':');
                                        me = int.Parse(line.Split(':')[1]);
                                    }
                                }
                                catch (Exception ex)
                                {
                                }
                                if (line.Contains("MoveEvent0"))
                                {
                                    parts = line.Split('-');
                                    symbol = parts[0].Split(':')[2].Replace(" ", "");
                                    locoID = parts[1];
                                    var track = parts[2].Split(' ')[0].Split(':')[1];
                                    bool inListTracks = ListTracks.Any(t => t.UID == int.Parse(track));
                                    if (ListTracks.Any(x => x.UID == int.Parse(track)))
                                    {
                                        if (!ListOfTrains.Any(x => x.Contains($"{symbol}|{locoID}")) && ListTracks.Any(t => t.UID == int.Parse(track)))
                                        {
                                            ListOfTrains.Add($"{symbol}|{locoID}|{dt}");
                                        }
                                        if (!ListSymbolMoves.ContainsKey($"{symbol}|{locoID}"))
                                        {
                                            ListSymbolMoves.Add($"{symbol}|{locoID}", $"{dt}~{int.Parse(track)}");
                                        }
                                        else
                                        {
                                            if (!ListSymbolMoves.ContainsValue($"{dt}~{int.Parse(track)}"))
                                            {
                                                var oldValue = ListSymbolMoves[$"{symbol}|{locoID}"];
                                                ListSymbolMoves[$"{symbol}|{locoID}"] = $"{oldValue}|{dt}~{int.Parse(track)}";
                                            }
                                        }
                                        if (DictOfTrackOsIndications.ContainsKey(int.Parse(track)))
                                        {
                                            DictOfTrackOsIndications[int.Parse(track)] = true;
                                        }
                                    }
                                }
                            }
                        }
                        stopwatch.Stop();
                        Console.WriteLine($".  Completed in {stopwatch.ElapsedMilliseconds} ms.  Added {count} symbol/locoID's to list.");
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}
