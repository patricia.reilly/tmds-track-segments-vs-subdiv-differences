using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BOS_Logs.GlobalDeclarations;

namespace BOS_Logs
{
    public class tblMoves
    {
        public DateTime EventTime { get; set; }
        public string UID { get; set; }
        public string TrkName { get; set; }
        public string Event { get; set; }
        public string SysData { get; set; }
    }
}
